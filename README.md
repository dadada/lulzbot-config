# Lulzbot Config

This repo contains configuration files for the [Lulzboti](https://stratum0.org/wiki/LulzBot).

To set up Octoprint with Klipper follow the following stepts:

- https://octoprint.org/download/
- https://www.klipper3d.org/Installation.html
- https://www.klipper3d.org/Config_checks.html

`printer.cfg` contains the appropriate configuration for Klipper.
